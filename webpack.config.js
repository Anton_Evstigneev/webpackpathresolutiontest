// Imports
const pathLib = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

// Module constants
const ENV_DEV = 'development';

// Webpack configuration
module.exports = {
    mode: ENV_DEV,
    entry: pathLib.resolve(__dirname, './cartridgeCore/client/js/app.js'),
    output: {
        filename: 'app_bundle.js',
        path: pathLib.resolve(__dirname, './cartridgeCore/static/js')
    },
    module: {
        rules: [
            // Babel stuff
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: [
                    {
                        // babel config is read from ".babelrc"
                        loader: 'babel-loader?cacheDirectory',
                    },
                ]
            },
            // ALL SASS/CSS code (from Vue components too) are extracted into separate file- "app_bundle.css"
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: { sourceMap: true, minimize: true }
                        }, {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true
                            }
                        }
                    ]
                }),
            },
        ],
    },
    plugins: [
        // path is relative to js bundle path. Absolute paths seems not to work as expected
        new ExtractTextPlugin('../css/app_bundle.css'),
    ],
    devtool: 'source-map',
    resolve: {
        modules: [
            // adding support for non-relative path imports in js
            'node_modules',
            pathLib.resolve('./cartridgeOverload/client'),
            pathLib.resolve('./cartridgeCore/client')
        ]
    },
    stats: 'normal'
};