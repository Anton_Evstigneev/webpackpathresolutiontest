Cartridge-wise intelligent FD build with Webpack

This was inspired by recent SFRA discussion.

So, everywhere on the backend we have this nice "overloading" modules feature:
require('*/someCartridge/somescript.js');
Same functionality for ISML templates using "isinclude".

This provides the strategy to extend(overload) parts of the code, not to "copy-edit" them.
Why is this so nice? Well, it allows to create a "plugin" cartridges: when the cartridge is added to some site, site's functionality changes.
But you only need to remove the cartridge from site to revert anything back. But what's really important such approach allows code base to be 100% DRY.
"copy-edit", on the other hand, bloats the code base and sometimes splits it in half.
Typical situation from the real project: you have a working site and the client wants to implement a "localized" version of it.
So dev team creates a new site, new cartridge(let's name it "newFeatures") in BM,
adds that cartridge to a site path before the legacy cartridge, and refactor legacy code to meet new client desires.
If you need some changes in ISML you just need to create a copy of some template in "newFeatures" and it will be resolved first instead of the legacy one.
This allows to use common codebase both for legacy and new site that has "replacements" only in the parts that are actually changed.

So if the legacy team fixed some bug in a legacy code (assuming you didn't change it in "newFeatures") your team will automatically get the
fixes in "newFeatures".

Notice that we're talking only about backend here. Because on the frontend side things are not so shiny.
You need to explicitly tell your JS/SCSS build tool(if it has support for it) how, where, and in what order to resolve JS/SCSS imports.
And it happens so that Webpack does it just fine with only one simple config option:


...
resolve: {
        modules: [
            // adding support for non-relative path imports in js
            'node_modules',
            pathLib.resolve('./cartridgeOverload/client'),
            pathLib.resolve('./cartridgeCore/client')
        ]
    },
...

Let's imagine that we have a js file with name "module1.js" in both "cartridgeCore/client/js" and "cartridgeOverload/client/js".
Then if we write:

import { something } from 'js/module1';

"module1" will first be searched in "node_modules/client/js", then in "cartridgeOverload/client/js" and then in "cartridgeCore/client/js".
And it will be imported from "cartridgeOverload" because it appears in a list before "cartridgeCore".
But it takes only to delete the "cartridgeOverload" item from the modules and "module1" will be imported from "cartridgeCore".
So we can easily get the same resolving mechanism as on the backend.

I've created a small repository at
https://bitbucket.org/Anton_Evstigneev/webpackpathresolutiontest/src
, just to demonstrate this feature

Try to comment/uncomment the
pathLib.resolve('./cartridgeOverload/client'),
string, then running "npm start"
and see how the build changes "cartridgeCore/static/js/app_bundle.js"(run it with Node to receive some console.log) and "cartridgeCore/static/css/app_bundle.css".